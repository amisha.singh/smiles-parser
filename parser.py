import re
import pandas as pd
def cal_follow(s, productions, first):
    follow = set()
    if(s == list(productions.keys())[0]):
      follow.add('$') 
    for i in productions.keys():
      for j in range(len(productions[i])):
        if(s in productions[i][j]):
          idx = productions[i][j].index(s)
          if(productions[i][j][idx] != productions[i][j][-1] and productions[i][j][idx+1] not in productions.keys()):            
            follow.add(productions[i][j][idx+1])  
          elif(productions[i][j][idx] == productions[i][j][-1]):
            if(productions[i][j][idx] == i):
              break
            else:

              f=cal_first(i,productions)
              for k in f:
                follow.add(k)
          else:
            while(idx+1 != len(productions[i][j]) and productions[i][j][idx+1] in productions.keys()):
              idx += 1
              f = cal_first(productions[i][j][idx], productions)       
              if('ε' not in f):
                for elements in f:
                  follow.add(elements)
                  
              elif('ε' in f and idx != len(productions[i][j])-1):
                f.remove('ε')
                for k in f:
                  follow.add(k)  
                if(productions[i][j][idx+1] in productions.keys()):
                  f=cal_first(productions[i][j][idx+1],productions)
                  for k in f:
                    follow.add(k)
                else:
                  follow.add(productions[i][j][idx+1])                          
    return follow
def cal_first(s, productions):
    
    first = set()
    
    for i in range(len(productions[s])):
        for j in range(len(productions[s][i])):
            
            c = productions[s][i][j]
           
            if(c in productions.keys() and c!=s):
              f = cal_first(c, productions)
              if('ε' not in f):
                  for k in f:
                      first.add(k)
                  break
              else:
                  if(j == len(productions[s][i])-1):
                      for k in f:
                          first.add(k)
                  else:
                      f.remove('ε')
                      for k in f:
                          first.add(k)
            elif(c==s):
              pass
            else:
              first.add(c)
            
              break   
    
    return first

def parsing_table(productions, first, follow):
    
    print("\nParsing Table\n")

    table = {}
    for key in productions:
      for value in productions[key]:
        val = ' '.join(value)
        if val!=key:
          if val != "ε":
            for element in first[key]:
              if(element != "ε"):
                if(val.split(' ', -1)[0] not in productions.keys()) :
                  if(element in val.split()):
                    table[key, element] = val
                  else:
                    pass
                else: 
                  if element in first[val.split(' ', -1)[0]]:
                    table[key, element] = val
         # else:
            #for element in follow[key]:
              #table[key, element] = val

    for key,val in table.items():
        print(key,"=>",val)

    new_table = {}
    for pair in table:
        new_table[pair[1]] = {}

    for pair in table:
        new_table[pair[1]][pair[0]] = table[pair]


    print("\n")
    print("\nParsing Table in matrix form\n")
    print(pd.DataFrame(new_table).fillna('-'))
    print("\n")
    return table


def check_validity(string, start, table,follow,productions,first):
    
    accepted = True
    
    input_string = string + '&'
    stack = []
    
    stack.append('&')
    stack.append(start)
    
    idx = 0
    print("Stack\t\tInput\t\tMoves") 
    while (len(stack) > 0):
       
      top = stack[-1]
      if top != '&':
        print(f"Top => {top}") 
      curr_string = input_string[idx]
     # print("current",curr_string)
     # print(idx,len(input_string))
      if curr_string=='&' and idx+1==len(input_string):
        accepted=True
        break
      if input_string[idx-1]+curr_string in ['Cl','Br','At','Ts','se','as']:
        #print("here")
        curr_string= input_string[idx-1]+curr_string
        #print("done")
        idx=idx+1
        #print(input_string[idx])
        if top=='Z':
          idx=idx
          curr_string=input_string[idx]
          #print("NOW",input_string[idx])
      if curr_string+input_string[idx+1] in ['Cl','Br','At','Ts','se','as']:
        #print("me")
        curr_string= curr_string+input_string[idx+1]
        idx=idx
        if input_string[idx+1]=='-':
          idx=idx+1+1
      if curr_string != '&':
        print(f"Current input => {curr_string}")
      if len(stack)==1 and idx<len(input_string)-1:
        stack.append('body')
        print("1")
        top=stack[-1]
      if curr_string=='&':
        accepted=True
        break
      if top == curr_string:
        stack.pop()
        idx += 1
        if len(stack)==1 and idx<len(input_string)-1:
          top="body"
          print("2")
      else:
        key = (top, curr_string)
        print(f"Key => {key}")
        if key not in table and len(stack)==1:
          top='body'
          print("3")
          key = (top,curr_string)
          if top==curr_string:
            stack.pop()
            idx+=1
        if key not in table and top in productions.keys() and input_string[idx-2]+input_string[idx-1] in follow[top]:
          stack.pop()
          print("Try exempting ",top)
          top=stack[-1]
          key=(top,curr_string)
          if key not in table and top in productions.keys():
            accepted = False
            print("Invalid literal =",curr_string)
            break
          elif key not in table and top=='&':
            accepted = False
            print("Invalid literal =",curr_string)
            break
        if key not in table and top in productions.keys() and input_string[idx-1] in follow[top]:
          stack.pop()
          print("Try exempting ",top)
          top=stack[-1]
          key=(top,curr_string)
          if key not in table and top in productions.keys():
            accepted = False
            print("Invalid literal =",curr_string)
            break
          elif key not in table and top=='&':
            accepted = False
            print("Invalid literal =",curr_string)
            break

        if idx==len(input_string):
          accepted = False
          break
        if top==curr_string=='&':
          accepted =True
          break
        if top!=curr_string and top in productions.keys(): 
          value = table[key]
        if top in productions.keys() and value != "ε" and top!=curr_string:
                
          value = value.split(' ')

          value = value[::-1]
                
          stack.pop()
          for ele in value:
            stack.append(ele)
        elif idx<len(input_string)+1 and top!=curr_string and curr_string in first['body']:
          print("elongate")
          stack.append("body")  
            
        elif top!=curr_string:
          print("pop")
          stack.pop()
    
    if accepted:
      predict=1
    else:
      predict=0
    return predict
def mean_squared_error(act, pred):
 
   diff = pred - 1
   differences_squared = diff ** 2
   mean_diff = differences_squared.mean()
    
   return mean_diff
def main():
    productions = {}
    grammar = open("grammar2.txt", "r")
    first = {}
    follow = {}
    table = {}  
    start = ""
    for prod in grammar:
        l = re.split(" > |\n| ", prod)
        m = []
        for i in l:
            if (i == "" or i == None or i == '\n' or i == " " or i == ">"):
                pass
            else:
                m.append(i)
        left_prod = m.pop(0)
        right_prod = []
        t = []
        for j in m:
            if(j != '|'):
                t.append(j)
            else:
                right_prod.append(t)
                t = []
              
        right_prod.append(t)
        productions[left_prod] = right_prod
        
        if(start == ""):
            start = left_prod
    print("*****GRAMMAR*****")    
    for lhs, rhs in productions.items():
        print(lhs, ":" , rhs)
    print("")
    
    for s in productions.keys():
      first[s] = cal_first(s, productions)
    print("*****FIRST*****") 
    for lhs, rhs in first.items():
        print(lhs, ":" , rhs)
    
    print("")
    for s in productions.keys():
        follow[s] = cal_follow(s, productions, first)
    
    print("*****FOLLOW*****")
    for lhs, rhs in follow.items():
      
        print(lhs, ":" , rhs)
    table = parsing_table(productions, first, follow)
    pred=np.array([])
   # for i in smiles:
   #   ans=check_validity(i, start, table,follow,productions)
   #   print("SMILES:",i,"ANSWER=",ans)
   #   pred=np.append(pred,ans)
   # print(pred)
   # print(mean_squared_error(1,pred))
    ans=check_validity(input(), start, table,follow,productions,first)
    print("SMILES:","ANSWER=",ans)
    grammar.close()
import numpy as np
import sys
sys.setrecursionlimit(10000)
if __name__ == "__main__":
  main()
  
