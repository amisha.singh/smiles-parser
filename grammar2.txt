smiles > atom body
body > branch | split | union | body | ε
branch > ( L )
L > smiles | atom | K smiles | L | ε
K > dot | bond | ε
split > dot smiles
union > bond X | X
X > smiles | rnum
atom > organic | bracket | star
bracket > [ A ]
A > isotope chiral E hcount charge | map | E Z
Z > Y | hcount charge | charge
chiral > @ @ | @ | ε
E > bracket_aromatic | star 
isotope > digit digit digit | digit digit | digit | ε
hcount > H digit | H | ε
charge > fifteen Y | ε
Y > + | - | ε
map > : digit digit digit digit | : digit digit digit | : digit digit | : digit
rnum > digit | % digit digit
organic > B | Br | C | Cl | N | O | P | S | F | I | At | Ts | b | c | n | o | p | s
bracket_aromatic > b | c | n | o | p | se | s | as | organic
bond > = | # | $ | / | \
fifteen > 1 T | M
T > 0 | 1 | 2 | 3 | 4 | 5 | ε
M > 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
digit > M | 0 | 1
dot > .
star > *
